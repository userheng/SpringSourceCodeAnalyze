package com.heng.ssc.analyze.enviroment.kernal;

/**
 * 提供属性访问功能：
 * 1.通过给定的key获取value <p>  
 * 2.解析文本中的占位符${... } <p>
 * @author hength20953
 *
 */
public interface PropertyResolver {

	/**
	 *	if the value for the given key is not {@code null}.
	 */
	boolean containsProperty(String key);

	/**
	 * Return the property value associated with the given key, or {@code null} if
	 * the key cannot be resolved.
	 */
	String getProperty(String key);
	
	String getProperty(String key, String defaultValue);
	
	<T> T getProperty(String key, Class<T> targetType);
	
	<T> T getProperty(String key, Class<T> targetType, T defaultValue);
	
	String getRequiredProperty(String key) throws IllegalStateException;
	
	<T> T getRequiredProperty(String key, Class<T> targetType) throws IllegalStateException;
	
	/**
	 * Resolve ${...} placeholders in the given text, replacing them with corresponding
	 * property values as resolved by {@link #getProperty}. 
	 * Unresolvable placeholders with no default value 
	 * are ignored and passed through unchanged.
	 */
	String resolvePlaceholders(String text);
	
	/**
	 * Unresolvable placeholders with no default value
	 * will cause an IllegalArgumentException to be thrown.
	 */
	String resolveRequiredPlaceholders(String text) throws IllegalArgumentException;
}
