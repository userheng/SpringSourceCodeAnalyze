package com.heng.ssc.analyze.enviroment.kernal;

import com.heng.ssc.analyze.enviroment.property.MapPropertySource;
import com.heng.ssc.analyze.enviroment.property.MutablePropertySources;
import com.heng.ssc.analyze.enviroment.property.SystemEnvironmentPropertySource;

public class StandardEnvironment extends AbstractEnvironment {

	/** System environment property source name: {@value} */
	public static final String SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME = "systemEnvironment";

	/** JVM system properties property source name: {@value} */
	public static final String SYSTEM_PROPERTIES_PROPERTY_SOURCE_NAME = "systemProperties";


	/**
	 * Customize the set of property sources with those appropriate for any standard
	 * Java environment:
	 */
	@Override
	protected void customizePropertySources(MutablePropertySources propertySources) {
		propertySources.addLast(new MapPropertySource(SYSTEM_PROPERTIES_PROPERTY_SOURCE_NAME, getSystemProperties()));
		propertySources.addLast(new SystemEnvironmentPropertySource(SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME, getSystemEnvironment()));
	}
	
	public static void main(String[] args) {
		StandardEnvironment standardEnvironment = new StandardEnvironment();
		System.out.println(standardEnvironment);
	}

}
