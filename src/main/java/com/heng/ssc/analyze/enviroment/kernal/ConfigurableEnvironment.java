package com.heng.ssc.analyze.enviroment.kernal;

import java.util.Map;

import com.heng.ssc.analyze.enviroment.property.MutablePropertySources;

/**
 * 提供激活profiles的功能
 * <p>
 *继承的功能{@link Environment} {@link ConfigurablePropertyResolver}
 */
public interface ConfigurableEnvironment extends Environment, ConfigurablePropertyResolver {

	void setActiveProfiles(String... profiles);
	
	void addActiveProfile(String profile);
	
	void setDefaultProfiles(String... profiles);
	
	MutablePropertySources getPropertySources();
	
	Map<String, Object> getSystemEnvironment();
	
	Map<String, Object> getSystemProperties();
	
	void merge(ConfigurableEnvironment parent);
}
