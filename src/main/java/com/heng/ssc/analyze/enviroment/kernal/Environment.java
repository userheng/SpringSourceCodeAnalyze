package com.heng.ssc.analyze.enviroment.kernal;

/**
 * 
 *提供访问和判断profiles功能
 *<p>继承的功能 {@link PropertyResolver}
 */
public interface Environment extends PropertyResolver{

	/**
	 * Return the set of profiles explicitly made active for this environment.
	 * <p>
	 * Profiles can be activated by setting "spring.profiles.active" as a system property
	 * or by calling setActiveProfiles(String...)
	 * <p>
	 * If no profiles have explicitly been specified as active, then any 
	 * default profiles will automatically be activated.
	 */
	String[] getActiveProfiles();
	
	/**
	 * Return the set of profiles to be active by default when no active profiles have
	 * been set explicitly.
	 */
	String[] getDefaultProfiles();
	
	/**
	 * Return whether one or more of the given profiles is active or, in the case of no
	 * explicit active profiles, whether one or more of the given profiles is included in
	 * the set of default profiles.
	 * <p>
	 * If a profile begins with '!' the logic is inverted,
	 * For example, <pre class="code">env.acceptsProfiles("p1", "!p2")</pre> will
	 * return {@code true} if profile 'p1' is active or 'p2' is not active.
	 */
	boolean acceptsProfiles(String... profiles);
}
