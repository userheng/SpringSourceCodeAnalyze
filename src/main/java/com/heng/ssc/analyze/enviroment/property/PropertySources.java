package com.heng.ssc.analyze.enviroment.property;

/**
 * 提供功能:
 * 1.根据name判断是否存在ProprtySource
 * 2.根据name获取ProprtySource
 * @author hength20953
 *
 */
public interface PropertySources extends Iterable<PropertySource<?>> {

	/**
	 * Return whether a property source with the given name is contained.
	 * @param name the {@linkplain PropertySource#getName() name of the property source} to find
	 */
	boolean contains(String name);

	/**
	 * Return the property source with the given name, {@code null} if not found.
	 * @param name the {@linkplain PropertySource#getName() name of the property source} to find
	 */
	PropertySource<?> get(String name);
}
