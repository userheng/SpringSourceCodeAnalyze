package com.heng.ssc.analyze.convert.suport;

import com.heng.ssc.analyze.core.TypeDescriptor;

public interface ConversionService {

	/**
	 * Return true if objects of sourceType can be converted to the targetType. 
	 * <p>
	 * Special note on collections, arrays, and maps types:
	 * For conversion between collection, array, and map types, this method will return {@code true}
	 * even though a convert invocation may still generate a {@link ConversionException} if the
	 * underlying elements are not convertible. Callers are expected to handle this exceptional case
	 * when working with collections and maps.
	 */
	boolean canConvert(Class<?> sourceType, Class<?> targetType);
	
	
	/**
	 * The TypeDescriptors provide additional context about the source and target locations where conversion would occur, 
	 * often object fields or property locations. 
	 */
	boolean canConvert(TypeDescriptor sourceType, TypeDescriptor targetType);
	
	
	<T> T convert(Object source, Class<T> targetType);
	
	Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType);
}
