package com.heng.ssc.analyze.proxy;

import java.io.Serializable;
import java.lang.reflect.Proxy;

import com.heng.ssc.analyze.proxy.suport.advice.AdvisedSupport;

/**
 * JDK 动态代理基于接口，所以只有接口中的方法会被增强，而 CGLIB 基于类继承，需要注意就是如果方法使用了 final 修饰，或者是 private 方法，是不能被增强的。
 * @author hength20953
 * @date:   2019年4月10日 下午4:02:34
 */
@SuppressWarnings("serial")
public class DefaultAopProxyFactory implements AopProxyFactory, Serializable {

	@Override
	public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {
		//(optimize默认false) || (proxy-target-class=true) || (没有接口)
		//proxy-target-class=true直接代理 	CGLIB是直接代理，JDK是间接？
		if (config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config)) {
			Class<?> targetClass = config.getTargetClass();
			if (targetClass == null) {
				throw new AopConfigException("TargetSource cannot determine target class: " +
						"Either an interface or a target is required for proxy creation.");
			}
			//如果要代理的类本身就是接口，也会用 JDK 动态代理
			if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
				return new JdkDynamicAopProxy(config);
			}
			
//			return new ObjenesisCglibAopProxy(config);
			return null;
		}
		else {
			return new JdkDynamicAopProxy(config);
		}
	}
	
	private boolean hasNoUserSuppliedProxyInterfaces(AdvisedSupport config) {
		Class<?>[] ifcs = config.getProxiedInterfaces();
		return (ifcs.length == 0 || (ifcs.length == 1 && SpringProxy.class.isAssignableFrom(ifcs[0])));
	}

}
