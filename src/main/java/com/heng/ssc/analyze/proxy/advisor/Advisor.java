package com.heng.ssc.analyze.proxy.advisor;

import com.heng.ssc.analyze.proxy.advice.Advice;

public interface Advisor {

	Advice getAdvice();
	
	boolean isPerInstance();
}
