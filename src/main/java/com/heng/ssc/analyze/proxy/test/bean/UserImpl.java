package com.heng.ssc.analyze.proxy.test.bean;

public class UserImpl implements IUser {

	@Override
	public User createUser(String username, int age) {
		return new User(username, age);
	}

	@Override
	public void printUser(User user) {
		System.out.println(user.toString());
	}

}
