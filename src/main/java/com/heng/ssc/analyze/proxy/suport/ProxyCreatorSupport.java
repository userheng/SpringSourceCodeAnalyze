package com.heng.ssc.analyze.proxy.suport;

import java.util.LinkedList;
import java.util.List;

import com.heng.ssc.analyze.proxy.AopProxy;
import com.heng.ssc.analyze.proxy.AopProxyFactory;
import com.heng.ssc.analyze.proxy.DefaultAopProxyFactory;
import com.heng.ssc.analyze.proxy.suport.advice.AdvisedSupport;
import com.heng.ssc.analyze.proxy.suport.advice.AdvisedSupportListener;

@SuppressWarnings("serial")
public class ProxyCreatorSupport extends AdvisedSupport {

	/** Set to true when the first AOP proxy has been created */
	private boolean active = false;

	private final List<AdvisedSupportListener> listeners = new LinkedList<AdvisedSupportListener>();

	private AopProxyFactory aopProxyFactory;

	/**
	 * Create a new ProxyCreatorSupport instance.
	 */
	public ProxyCreatorSupport() {
		this.aopProxyFactory = new DefaultAopProxyFactory();
	}

	/**
	 * 创建一个 AopProxy 的实例
	 * 
	 * @return
	 */
	protected final synchronized AopProxy createAopProxy() {
		if (!this.active) {
			activate();
		}
		return getAopProxyFactory().createAopProxy(this);
	}

	private void activate() {
		this.active = true;
		for (AdvisedSupportListener listener : this.listeners) {
			listener.activated(this);
		}
	}

	public AopProxyFactory getAopProxyFactory() {
		return this.aopProxyFactory;
	}
}
