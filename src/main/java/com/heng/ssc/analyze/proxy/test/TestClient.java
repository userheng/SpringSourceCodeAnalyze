package com.heng.ssc.analyze.proxy.test;

import com.heng.ssc.analyze.proxy.ProxyFactory;
import com.heng.ssc.analyze.proxy.advisor.RegexpMethodPointcutAdvisor;
import com.heng.ssc.analyze.proxy.suport.advice.SingletonTargetSource;
import com.heng.ssc.analyze.proxy.test.advice.LogAfterAdvice;
import com.heng.ssc.analyze.proxy.test.advice.LogBeforeAdvice;
import com.heng.ssc.analyze.proxy.test.bean.IUser;
import com.heng.ssc.analyze.proxy.test.bean.UserImpl;
import com.heng.ssc.analyze.util.ClassUtils;

public class TestClient {

	public static void main(String[] args) {
		
		IUser iUser = new UserImpl();
		
		ProxyFactory proxyFactory = new ProxyFactory();
		//proxyTargetClass=false; optimize=false; opaque=false; exposeProxy=false; frozen=false
		
		Class<?>[] targetInterfaces = ClassUtils.getAllInterfacesForClass(UserImpl.class, ClassUtils.getDefaultClassLoader());
		for (Class<?> ifc : targetInterfaces) {
			proxyFactory.addInterface(ifc);
		}
		
		LogBeforeAdvice logBeforeAdvice = new LogBeforeAdvice();
		LogAfterAdvice logAfterAdvice = new LogAfterAdvice();
		
		RegexpMethodPointcutAdvisor logBeforeAdvisor = new RegexpMethodPointcutAdvisor();
		logBeforeAdvisor.setAdvice(logBeforeAdvice);
		logBeforeAdvisor.setPattern("com.heng.ssc.analyze.proxy.test.bean.*.create.*");
		
		RegexpMethodPointcutAdvisor logAfterAdvisor = new RegexpMethodPointcutAdvisor();
		logAfterAdvisor.setAdvice(logAfterAdvice);
		logAfterAdvisor.setPattern("com.heng.ssc.analyze.proxy.test.bean.*.create.*");
		
		proxyFactory.addAdvisors(logBeforeAdvisor, logAfterAdvisor);
		proxyFactory.setTargetSource(new SingletonTargetSource(iUser));
		
//		proxyFactory.setPreFiltered(true);
		
		IUser proxy = (IUser)proxyFactory.getProxy(ClassUtils.getDefaultClassLoader());
		
		proxy.createUser("hength", 25);
		
	}
}
