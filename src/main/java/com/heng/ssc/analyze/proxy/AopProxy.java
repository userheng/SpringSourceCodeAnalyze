package com.heng.ssc.analyze.proxy;

public interface AopProxy {

	Object getProxy();
	
	Object getProxy(ClassLoader classLoader);
}
