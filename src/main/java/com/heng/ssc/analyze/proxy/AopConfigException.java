package com.heng.ssc.analyze.proxy;

import com.heng.ssc.analyze.exception.NestedRuntimeException;

@SuppressWarnings("serial")
public class AopConfigException extends NestedRuntimeException {

	public AopConfigException(String msg) {
		super(msg);
	}

	public AopConfigException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
