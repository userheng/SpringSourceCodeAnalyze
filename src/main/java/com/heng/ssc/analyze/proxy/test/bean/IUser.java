package com.heng.ssc.analyze.proxy.test.bean;

public interface IUser {

	User createUser(String username, int age);
	
	void printUser(User user);
}
