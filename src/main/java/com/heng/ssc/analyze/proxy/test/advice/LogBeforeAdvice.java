package com.heng.ssc.analyze.proxy.test.advice;

import java.lang.reflect.Method;
import java.util.Arrays;

import com.heng.ssc.analyze.proxy.advice.MethodBeforeAdvice;

public class LogBeforeAdvice implements MethodBeforeAdvice {

	@Override
	public void before(Method method, Object[] args, Object target) throws Throwable {
		System.out.println(
				"准备执行方法：" + target.getClass().getName() + "#" + method.getName() + ",参数列表：" + Arrays.toString(args));
	}

}
