package com.heng.ssc.analyze.proxy.suport.advice;


public interface AdvisedSupportListener {
	
	void activated(AdvisedSupport advised);

	void adviceChanged(AdvisedSupport advised);
}
