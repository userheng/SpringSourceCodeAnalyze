package com.heng.ssc.analyze.proxy;

import com.heng.ssc.analyze.proxy.suport.advice.AdvisedSupport;

public interface AopProxyFactory {

	AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException;
}
