package com.heng.ssc.analyze.proxy;

import com.heng.ssc.analyze.proxy.suport.ProxyCreatorSupport;

@SuppressWarnings("serial")
public class ProxyFactory extends ProxyCreatorSupport{

	/**
	 * Create a new ProxyFactory.
	 */
	public ProxyFactory() {
	}
	
	public Object getProxy(ClassLoader classLoader) {
		return createAopProxy().getProxy(classLoader);
	}
}
