package com.heng.ssc.analyze.proxy;

import com.heng.ssc.analyze.exception.NestedRuntimeException;

@SuppressWarnings("serial")
public class AopInvocationException extends NestedRuntimeException {

	/**
	 * Constructor for AopInvocationException.
	 * @param msg the detail message
	 */
	public AopInvocationException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for AopInvocationException.
	 * @param msg the detail message
	 * @param cause the root cause
	 */
	public AopInvocationException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
