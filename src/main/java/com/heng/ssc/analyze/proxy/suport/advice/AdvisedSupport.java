package com.heng.ssc.analyze.proxy.suport.advice;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.heng.ssc.analyze.proxy.AopConfigException;
import com.heng.ssc.analyze.proxy.advice.Advice;
import com.heng.ssc.analyze.proxy.advisor.Advisor;
import com.heng.ssc.analyze.proxy.advisor.AdvisorChainFactory;
import com.heng.ssc.analyze.proxy.advisor.DefaultAdvisorChainFactory;
import com.heng.ssc.analyze.proxy.advisor.DefaultPointcutAdvisor;
import com.heng.ssc.analyze.proxy.advisor.IntroductionAdvisor;
import com.heng.ssc.analyze.util.Assert;
import com.heng.ssc.analyze.util.ClassUtils;
import com.heng.ssc.analyze.util.CollectionUtils;

public class AdvisedSupport extends ProxyConfig implements Advised {

	/** use serialVersionUID from Spring 2.0 for interoperability */
	private static final long serialVersionUID = 2651364800145442165L;

	/**
	 * Canonical TargetSource when there's no target, and behavior is
	 * supplied by the advisors.
	 */
	public static final TargetSource EMPTY_TARGET_SOURCE = EmptyTargetSource.INSTANCE;
	
	/** Package-protected to allow direct access for efficiency */
//	TargetSource targetSource = EMPTY_TARGET_SOURCE;
	public TargetSource targetSource = EMPTY_TARGET_SOURCE;
	
	/**
	 * Interfaces to be implemented by the proxy. Held in List to keep the order
	 * of registration, to create JDK proxy with specified order of interfaces.
	 */
	private List<Class<?>> interfaces = new ArrayList<Class<?>>();
	
	private boolean proxyTargetClass = false;
	
	/** Cache with Method as key and advisor chain List as value */
	private transient Map<MethodCacheKey, List<Object>> methodCache;
	
	/** The AdvisorChainFactory to use */
	AdvisorChainFactory advisorChainFactory = new DefaultAdvisorChainFactory();
	
	/**
	 * List of Advisors. If an Advice is added, it will be wrapped
	 * in an Advisor before being added to this List.
	 */
	private List<Advisor> advisors = new LinkedList<Advisor>();
	
	/**
	 * Array updated on changes to the advisors list, which is easier
	 * to manipulate internally.
	 */
	private Advisor[] advisorArray = new Advisor[0];
	
	/**
	 * No-arg constructor for use as a JavaBean.
	 */
	public AdvisedSupport() {
		this.methodCache = new ConcurrentHashMap<>(32);
	}
	
	@Override
	public Class<?> getTargetClass() {
		return this.targetSource.getTargetClass();
	}

	@Override
	public boolean isFrozen() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isProxyTargetClass() {
		return this.proxyTargetClass;
	}

	@Override
	public Class<?>[] getProxiedInterfaces() {
		return ClassUtils.toClassArray(this.interfaces);
	}

	@Override
	public boolean isInterfaceProxied(Class<?> intf) {
		for (Class<?> proxyIntf : this.interfaces) {
			if (intf.isAssignableFrom(proxyIntf)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setTargetSource(TargetSource targetSource) {
		this.targetSource = (targetSource != null ? targetSource : EMPTY_TARGET_SOURCE);
	}

	@Override
	public TargetSource getTargetSource() {
		return this.targetSource;
	}

	@Override
	public void setExposeProxy(boolean exposeProxy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isExposeProxy() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setPreFiltered(boolean preFiltered) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isPreFiltered() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Advisor[] getAdvisors() {
		return this.advisorArray;
	}
	
	/**
	 * Add all of the given advisors to this proxy configuration.
	 * @param advisors the advisors to register
	 */
	public void addAdvisors(Advisor... advisors) {
		addAdvisors(Arrays.asList(advisors));
	}
	
	/**
	 * Add all of the given advisors to this proxy configuration.
	 * @param advisors the advisors to register
	 */
	public void addAdvisors(Collection<Advisor> advisors) {
		if (isFrozen()) {
			throw new AopConfigException("Cannot add advisor: Configuration is frozen.");
		}
		if (!CollectionUtils.isEmpty(advisors)) {
			for (Advisor advisor : advisors) {
//				if (advisor instanceof IntroductionAdvisor) {
//					validateIntroductionAdvisor((IntroductionAdvisor) advisor);
//				}
				Assert.notNull(advisor, "Advisor must not be null");
				this.advisors.add(advisor);
			}
			updateAdvisorArray();
			adviceChanged();
		}
	}

	@Override
	public void addAdvisor(Advisor advisor) throws AopConfigException {
		int pos = this.advisors.size();
		addAdvisor(pos, advisor);
	}

	@Override
	public void addAdvisor(int pos, Advisor advisor) throws AopConfigException {
//		if (advisor instanceof IntroductionAdvisor) {
//			validateIntroductionAdvisor((IntroductionAdvisor) advisor);
//		}
		addAdvisorInternal(pos, advisor);
	}
	
	private void addAdvisorInternal(int pos, Advisor advisor) throws AopConfigException {
		Assert.notNull(advisor, "Advisor must not be null");
		if (isFrozen()) {
			throw new AopConfigException("Cannot add advisor: Configuration is frozen.");
		}
		if (pos > this.advisors.size()) {
			throw new IllegalArgumentException(
					"Illegal position " + pos + " in advisor list with size " + this.advisors.size());
		}
		this.advisors.add(pos, advisor);
		updateAdvisorArray();
		adviceChanged();
	}
	
	/**
	 * Bring the array up to date with the list.
	 */
	protected final void updateAdvisorArray() {
		this.advisorArray = this.advisors.toArray(new Advisor[this.advisors.size()]);
	}

	@Override
	public boolean removeAdvisor(Advisor advisor) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeAdvisor(int index) throws AopConfigException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int indexOf(Advisor advisor) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean replaceAdvisor(Advisor a, Advisor b) throws AopConfigException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addAdvice(Advice advice) throws AopConfigException {
		int pos = this.advisors.size();
		addAdvice(pos, advice);
	}

	@Override
	public void addAdvice(int pos, Advice advice) throws AopConfigException {
		Assert.notNull(advice, "Advice must not be null");
//		if (advice instanceof IntroductionInfo) {
//			// We don't need an IntroductionAdvisor for this kind of introduction:
//			// It's fully self-describing.
//			addAdvisor(pos, new DefaultIntroductionAdvisor(advice, (IntroductionInfo) advice));
//		}
//		else if (advice instanceof DynamicIntroductionAdvice) {
//			// We need an IntroductionAdvisor for this kind of introduction.
//			throw new AopConfigException("DynamicIntroductionAdvice may only be added as part of IntroductionAdvisor");
//		}
//		else {
//			addAdvisor(pos, new DefaultPointcutAdvisor(advice));
//		}
		
		addAdvisor(pos, new DefaultPointcutAdvisor(advice));
	}

	@Override
	public boolean removeAdvice(Advice advice) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int indexOf(Advice advice) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toProxyConfigString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Set the interfaces to be proxied.
	 */
	public void setInterfaces(Class<?>... interfaces) {
		Assert.notNull(interfaces, "Interfaces must not be null");
		this.interfaces.clear();
		for (Class<?> ifc : interfaces) {
			addInterface(ifc);
		}
	}

	/**
	 * Add a new proxied interface.
	 * @param intf the additional interface to proxy
	 */
	public void addInterface(Class<?> intf) {
		Assert.notNull(intf, "Interface must not be null");
		if (!intf.isInterface()) {
			throw new IllegalArgumentException("[" + intf.getName() + "] is not an interface");
		}
		if (!this.interfaces.contains(intf)) {
			this.interfaces.add(intf);
			adviceChanged();
		}
	}
	
	/**
	 * Invoked when advice has changed.
	 */
	protected void adviceChanged() {
		this.methodCache.clear();
	}
	
	public List<Object> getInterceptorsAndDynamicInterceptionAdvice(Method method, Class<?> targetClass) {
		MethodCacheKey cacheKey = new MethodCacheKey(method);
		List<Object> cached = this.methodCache.get(cacheKey);
		if (cached == null) {
			cached = this.advisorChainFactory.getInterceptorsAndDynamicInterceptionAdvice(
					this, method, targetClass);
			this.methodCache.put(cacheKey, cached);
		}
		return cached;
	}
	
	
	/**
	 * Simple wrapper class around a Method. Used as the key when
	 * caching methods, for efficient equals and hashCode comparisons.
	 */
	private static final class MethodCacheKey implements Comparable<MethodCacheKey> {

		private final Method method;

		private final int hashCode;

		public MethodCacheKey(Method method) {
			this.method = method;
			this.hashCode = method.hashCode();
		}

		@Override
		public boolean equals(Object other) {
			return (this == other || (other instanceof MethodCacheKey &&
					this.method == ((MethodCacheKey) other).method));
		}

		@Override
		public int hashCode() {
			return this.hashCode;
		}

		@Override
		public String toString() {
			return this.method.toString();
		}

		@Override
		public int compareTo(MethodCacheKey other) {
			int result = this.method.getName().compareTo(other.method.getName());
			if (result == 0) {
				result = this.method.toString().compareTo(other.method.toString());
			}
			return result;
		}
	}
}
