package com.heng.ssc.analyze.proxy.suport.advice;

public interface TargetSource extends TargetClassAware {

	@Override
	Class<?> getTargetClass();

	boolean isStatic();

	Object getTarget() throws Exception;

	void releaseTarget(Object target) throws Exception;

}
