package com.heng.ssc.analyze.proxy.advisor;


import com.heng.ssc.analyze.proxy.advisor.adpter.AdvisorAdapter;
import com.heng.ssc.analyze.proxy.invoke.MethodInterceptor;

public interface AdvisorAdapterRegistry {

	/**
	 * Return an Advisor wrapping the given advice.
	 * <p>Should by default at least support
	 * {@link org.aopalliance.intercept.MethodInterceptor},
	 * {@link com.heng.ssc.analyze.proxy.advice.aop.MethodBeforeAdvice},
	 * {@link com.heng.ssc.analyze.proxy.advice.aop.AfterReturningAdvice},
	 * {@link com.heng.ssc.analyze.proxy.advice.aop.ThrowsAdvice}.
	 * @param advice object that should be an advice
	 * @return an Advisor wrapping the given advice. Never returns {@code null}.
	 * If the advice parameter is an Advisor, return it.
	 * @throws UnknownAdviceTypeException if no registered advisor adapter
	 * can wrap the supposed advice
	 */
	Advisor wrap(Object advice) throws UnknownAdviceTypeException;

	/**
	 * Return an array of AOP Alliance MethodInterceptors to allow use of the
	 * given Advisor in an interception-based framework.
	 * <p>Don't worry about the pointcut associated with the Advisor,
	 * if it's a PointcutAdvisor: just return an interceptor.
	 * @param advisor Advisor to find an interceptor for
	 * @return an array of MethodInterceptors to expose this Advisor's behavior
	 * @throws UnknownAdviceTypeException if the Advisor type is
	 * not understood by any registered AdvisorAdapter.
	 */
	MethodInterceptor[] getInterceptors(Advisor advisor) throws UnknownAdviceTypeException;

	/**
	 * Register the given AdvisorAdapter. Note that it is not necessary to register
	 * adapters for an AOP Alliance Interceptors or Spring Advices: these must be
	 * automatically recognized by an AdvisorAdapterRegistry implementation.
	 * @param adapter AdvisorAdapter that understands a particular Advisor
	 * or Advice types
	 */
	void registerAdvisorAdapter(AdvisorAdapter adapter);

}
