package com.heng.ssc.analyze.proxy.test.advice;

import java.lang.reflect.Method;

import com.heng.ssc.analyze.proxy.advice.AfterReturningAdvice;

public class LogAfterAdvice implements AfterReturningAdvice{

	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("方法"+target.getClass().getName() + "#" + method.getName()+"返回："+ returnValue);
	}

}
