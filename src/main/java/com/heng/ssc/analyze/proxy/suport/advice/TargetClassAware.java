package com.heng.ssc.analyze.proxy.suport.advice;

public interface TargetClassAware {

	Class<?> getTargetClass();
}
